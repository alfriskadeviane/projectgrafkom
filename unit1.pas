unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Arrow, ComCtrls, ComboEx, Spin, Buttons, ColorBox;

type

  { TForm1 }
  Elemen=record
    x,y: integer;
  end;

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    atas: TBitBtn;
    bawah: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Reflection: TGroupBox;
    Rkanan: TBitBtn;
    Rkiri: TBitBtn;
    bLingkaran: TBitBtn;
    bTrapesium: TBitBtn;
    bSegitiga: TBitBtn;
    bPersegi: TBitBtn;
    kananbawah: TBitBtn;
    kiribawah: TBitBtn;
    kanan: TBitBtn;
    kananatas: TBitBtn;
    kiri: TBitBtn;
    Button1: TButton;
    Button19: TButton;
    Button2: TButton;
    Button20: TButton;
    Button21: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    ColorButton1: TColorButton;
    ComboBoxEx1: TComboBoxEx;
    Edit1: TEdit;
    Edit2: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    TrackBar1: TTrackBar;
    procedure atasClick(Sender: TObject);
    procedure bawahClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure bLingkaranClick(Sender: TObject);
    procedure bPersegiClick(Sender: TObject);
    procedure bSegitigaClick(Sender: TObject);
    procedure bTrapesiumClick(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure Button23Click(Sender: TObject);
    procedure Button24Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GroupBox9Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormShow(sender:TObject);
    procedure Edit(sender:TObject);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1Paint(Sender: TObject);
    procedure kananatasClick(Sender: TObject);
    procedure kananbawahClick(Sender: TObject);
    procedure kananClick(Sender: TObject);
    procedure kiribawahClick(Sender: TObject);
    procedure kiriClick(Sender: TObject);
    procedure RkananClick(Sender: TObject);
    procedure RkiriClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    Procedure FloodFill(x,y,fill,boundary:Integer);
    Procedure BoundaryFill(x,y,fill,boundary:Integer);
  private

  public

  end;

var
  Form1: TForm1;
  Objek: array[1..20] of elemen;
  temp: array[1..20] of elemen;
  tombol, i, perulangan, x1, y1, x2, y2, skew,m,n, j:integer;
  Gambar: boolean = False;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormActivate(Sender: TObject);
begin
  Image1.Canvas.Pen.Color:=clWhite;
  Image1.Canvas.Rectangle(0,0,Image1.width,Image1.Height);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

end;

procedure TForm1.GroupBox9Click(Sender: TObject);
begin

end;

procedure TForm1.Image1Click(Sender: TObject);
begin

end;

procedure TForm1.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  X1:=X;y1:=Y;
  if(tombol = 2) OR (tombol = 3) then
   begin
      Image1.Canvas.MoveTo(X,Y);
      Gambar := true;
   end;
  if(tombol = 4) then
   begin
    Image1.Canvas.Brush.Style:=bsSolid;
    Image1.Canvas.Brush.Color:=ColorButton1.ButtonColor;
    Image1.Canvas.FloodFill(x,y,Image1.Canvas.Pixels[x,y],fsSurface)
   end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
   tombol:=1;
   TrackBar1.Position := 1;
   Image1.Canvas.Pen.Width:=TrackBar1.Position;
   ColorButton1.ButtonColor:=clBlack;
   Image1.Canvas.Brush.Color:=clWhite;
   ColorButton1.ButtonColor:=clBlack;
   ComboBoxEx1.ItemIndex:= 0;
   Image1.Canvas.Pen.Style:=psSolid;
   Image1.Canvas.Pen.Color:=clWhite;
   Image1.Canvas.Rectangle(0,0,Image1.width,Image1.Height);
   Image1.Canvas.Pen.Color:=clBlack;
end;

procedure TForm1.Button20Click(Sender: TObject);
begin
  Button1Click(Sender);
  if(perulangan <> 2) then
  begin
  Objek[1].x := Objek[1].x+SpinEdit2.Value;
  Objek[perulangan].x := Objek[perulangan].x+SpinEdit2.Value;
    if(perulangan = 5) then
    begin
      Objek[2].x := Objek[2].x+SpinEdit2.Value;
    end;
  end;
  FormShow(sender);
end;

procedure TForm1.Button21Click(Sender: TObject);
begin
  Button1Click(Sender);
  if(perulangan <> 2) then
  begin

  Objek[3].y := Objek[3].y+SpinEdit2.Value;
    if(perulangan = 5) then
    begin
      Objek[2].y := Objek[2].y+SpinEdit2.Value;
    end;
  end;
  FormShow(sender);
end;



procedure TForm1.Button23Click(Sender: TObject);
begin
 if perulangan = 5 then
 begin
 Button1Click(Sender);
      Objek[1].x -=SpinEdit3.Value;        Objek[1].y -=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
      Objek[3].x +=SpinEdit3.Value;        Objek[3].y +=SpinEdit3.Value;
      Objek[4].x -=SpinEdit3.Value;        Objek[4].y +=SpinEdit3.Value;
      Objek[5].x -=SpinEdit3.Value;        Objek[5].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan =4 then
 begin
 Button1Click(Sender);
                                           Objek[1].y -=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
      Objek[3].x -=SpinEdit3.Value;        Objek[3].y +=SpinEdit3.Value;
                                           Objek[4].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 2 then
 begin
 Button1Click(Sender);
      Objek[1].x +=SpinEdit3.Value;        Objek[1].y -=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;

end;

procedure TForm1.Button24Click(Sender: TObject);
begin
 if perulangan = 5 then
 begin
 Button1Click(Sender);
      Objek[1].x +=SpinEdit3.Value;        Objek[1].y +=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
      Objek[3].x -=SpinEdit3.Value;        Objek[3].y -=SpinEdit3.Value;
      Objek[4].x +=SpinEdit3.Value;        Objek[4].y -=SpinEdit3.Value;
      Objek[5].x +=SpinEdit3.Value;        Objek[5].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 4 then
 begin
 Button1Click(Sender);
                                           Objek[1].y +=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
      Objek[3].x +=SpinEdit3.Value;        Objek[3].y -=SpinEdit3.Value;
                                           Objek[4].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 2 then
 begin
 Button1Click(Sender);
      Objek[1].x -=SpinEdit3.Value;        Objek[1].y +=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
   tombol:=1;
   If SaveDialog1.Execute then Image1.Picture.SaveToFile(SaveDialog1.FileName);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   tombol:=1;
    If OpenDialog1.Execute then Image1.Picture.LoadFromFile(OpenDialog1.FileName);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  tombol:=2;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  tombol:=4;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  tombol:=3;
end;



procedure TForm1.Edit2Change(Sender: TObject);
begin

end;



procedure TForm1.BitBtn1Click(Sender: TObject);
begin
   Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;  Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  if perulangan = 5 then
 begin
 Button1Click(Sender);
      Objek[1].x -=SpinEdit3.Value;        Objek[1].y -=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
      Objek[3].x +=SpinEdit3.Value;        Objek[3].y +=SpinEdit3.Value;
      Objek[4].x -=SpinEdit3.Value;        Objek[4].y +=SpinEdit3.Value;
      Objek[5].x -=SpinEdit3.Value;        Objek[5].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan =4 then
 begin
 Button1Click(Sender);
                                           Objek[1].y -=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
      Objek[3].x -=SpinEdit3.Value;        Objek[3].y +=SpinEdit3.Value;
                                           Objek[4].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 2 then
 begin
 Button1Click(Sender);
      Objek[1].x +=SpinEdit3.Value;        Objek[1].y -=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;

end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
   if perulangan = 5 then
 begin
 Button1Click(Sender);
      Objek[1].x +=SpinEdit3.Value;        Objek[1].y +=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y +=SpinEdit3.Value;
      Objek[3].x -=SpinEdit3.Value;        Objek[3].y -=SpinEdit3.Value;
      Objek[4].x +=SpinEdit3.Value;        Objek[4].y -=SpinEdit3.Value;
      Objek[5].x +=SpinEdit3.Value;        Objek[5].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 4 then
 begin
 Button1Click(Sender);
                                           Objek[1].y +=SpinEdit3.Value;
      Objek[2].x -=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
      Objek[3].x +=SpinEdit3.Value;        Objek[3].y -=SpinEdit3.Value;
                                           Objek[4].y +=SpinEdit3.Value;
 FormShow(Sender);
 end;
 if perulangan = 2 then
 begin
 Button1Click(Sender);
      Objek[1].x -=SpinEdit3.Value;        Objek[1].y +=SpinEdit3.Value;
      Objek[2].x +=SpinEdit3.Value;        Objek[2].y -=SpinEdit3.Value;
 FormShow(Sender);
 end;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
   if (tombol=5) OR (tombol= 6) then
  begin
    m:=Image1.Width;
    n:=Image1.Height div 2;
    Image1.Canvas.MoveTo(0,n);
    Image1.Canvas.LineTo(m,n);
    for i:=1 to 6 do
     begin
       temp[i].y:=(Objek[i].y-(2*(Objek[i].y-n)));
       temp[i].x:=Objek[i].x;
     end;
    Image1.Canvas.MoveTo(temp[6].x,temp[6].y);
    for i:=1 to 6 do
     begin
        Image1.Canvas.LineTo(temp[i].x,temp[i].y);
     end;
  end;

  if tombol=7 then
  begin
    m:=Image1.Width;
    n:=Image1.Height div 2;
    Image1.Canvas.MoveTo(0,n);
    Image1.Canvas.LineTo(m,n);
    for i:=1 to 6 do
     begin
       temp[i].y:=(Objek[i].y-(2*(Objek[i].y-n)));
       temp[i].x:=Objek[i].x;
     end;
    Image1.Canvas.MoveTo(temp[6].x,temp[6].y);
    for i:=1 to 6 do
     begin
        Image1.Canvas.LineTo(temp[i].x,temp[i].y);
     end;
  end;
end;

procedure TForm1.BitBtn5Click(Sender: TObject);
begin
  if (tombol=5) OR (tombol= 6) then
  begin
    m:=Image1.Width div 2;
    n:=Image1.Height ;
    Image1.Canvas.MoveTo(m,0);
    Image1.Canvas.LineTo(m,n);
    for i:=1 to 6 do
      begin
           temp[i].x:=(Objek[i].x-(2*(Objek[i].x-m)));
           temp[i].y:=Objek[i].y;
      end;
        Image1.Canvas.MoveTo(temp[6].x,temp[6].y);
        for i:=1 to 6 do
          begin
            Image1.Canvas.LineTo(temp[i].x,temp[i].y);
          end;
  end;
  if tombol=7 then
  begin
    m:=Image1.Width div 2;
    n:=Image1.Height ;
    Image1.Canvas.MoveTo(m,0);
    Image1.Canvas.LineTo(m,n);
    for i:=1 to 6 do
      begin
           temp[i].x:=(Objek[i].x-(2*(Objek[i].x-m)));
           temp[i].y:=Objek[i].y;
      end;
        Image1.Canvas.MoveTo(temp[6].x,temp[6].y);
        for i:=1 to 6 do
          begin
            Image1.Canvas.LineTo(temp[i].x,temp[i].y);
          end;
  end;
end;

procedure TForm1.bLingkaranClick(Sender: TObject);
begin
  tombol:=8; perulangan:=2;
end;

procedure TForm1.bPersegiClick(Sender: TObject);
begin
  tombol:=5; perulangan:=5;
end;

procedure TForm1.bSegitigaClick(Sender: TObject);
begin
  tombol:=7; perulangan:=4;
end;

procedure TForm1.bTrapesiumClick(Sender: TObject);
begin
  tombol:=6; perulangan:=5;
end;

procedure TForm1.atasClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.bawahClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button11Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;  Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button12Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button13Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;  Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button14Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button15Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;  Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button16Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button17Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;  Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button18Click(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.Button19Click(Sender: TObject);
begin
  tombol:=9;
end;

procedure TForm1.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Edit1.Text:=inttostr(X)+','+inttostr(Y);
  if (Gambar = true) then
   begin
      if(tombol = 2) then
       begin
          Image1.Canvas.Pen.Width:=TrackBar1.Position;
          Image1.Canvas.Pen.Color:=ColorButton1.ButtonColor;
       end;
      if(tombol = 3) then
       begin
          Image1.Canvas.Pen.Color:=clWhite;
          Image1.Canvas.Pen.Width:=10;
       end;
      Image1.Canvas.LineTo(X,Y);
   end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  Edit2.Text:= inttostr(TrackBar1.Position);
end;

procedure TForm1.Edit(sender:TObject);
begin
  Image1.Canvas.Pen.Width:=TrackBar1.Position;
  Image1.Canvas.Pen.Color:=ColorButton1.ButtonColor;
  if (ComboBoxEx1.ItemIndex = 0) then
  begin
     Image1.Canvas.Pen.Style:=psSolid;
  end;
  if (ComboBoxEx1.ItemIndex = 1) then
  begin
     Image1.Canvas.Pen.Style:=psDash;
  end;
  if (ComboBoxEx1.ItemIndex = 2) then
  begin
     Image1.Canvas.Pen.Style:=psDot;
  end;
  if (ComboBoxEx1.ItemIndex = 3) then
  begin
     Image1.Canvas.Pen.Style:=psDashDot;
  end;
  FormShow(sender);
end;

procedure TForm1.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  x2:=X;y2:=Y;
  Gambar:=false;
  if (tombol = 9) then
  begin
     ColorButton1.ButtonColor := Image1.Canvas.Pixels[x2, y2];
  end;
  if(tombol = 5) then
  begin
     Objek[1].x := x1;                  Objek[1].y := y1;
     Objek[2].x := x2;                  Objek[2].y := y1;
     Objek[3].x := x2;                  Objek[3].y := y2;
     Objek[4].x := x1;                  Objek[4].y := y2;
     Objek[5].x := x1;                  Objek[5].y := y1;
     Edit(sender);
  end;
  if(tombol = 6) then
  begin
     Objek[1].x := x1+((x2-x1) div 4);                  Objek[1].y := y1;
     Objek[2].x := x2-((x2-x1) div 4);                  Objek[2].y := y1;
     Objek[3].x := x2;                                  Objek[3].y := y2;
     Objek[4].x := x1;                                  Objek[4].y := y2;
     Objek[5].x := x1+((x2-x1) div 4);                  Objek[5].y := y1;
     Edit(sender);
  end;
  if(tombol = 7) then
  begin
     Objek[1].x := (x2+x1) div 2;                  Objek[1].y := y1;
     Objek[2].x := x2;                             Objek[2].y := y2;
     Objek[3].x := x1;                             Objek[3].y := y2;
     Objek[4].x := (x2+x1) div 2;                  Objek[4].y := y1;
     Edit(sender);
  end;
  if(tombol = 8) then
  begin
     Objek[1].x := x2;                  Objek[1].y := y1;
     Objek[2].x := x1;                  Objek[2].y := y2;
     Edit(sender);
  end;
end;

procedure TForm1.Image1Paint(Sender: TObject);
begin

end;

procedure TForm1.kananatasClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;  Objek[i].y := Objek[i].y-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.kananbawahClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;  Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.kananClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.kiribawahClick(Sender: TObject);
begin
   Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;  Objek[i].y := Objek[i].y+SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.kiriClick(Sender: TObject);
begin
  Button1Click(Sender);
  for i:=1 to perulangan do
   begin
      Objek[i].x := Objek[i].x-SpinEdit1.Value;
   end;
  FormShow(sender);
end;

procedure TForm1.RkananClick(Sender: TObject);
  var
    TempObjek: array[1..6] of elemen;
    n,m : integer;
    Sdt: real;
  begin
    Button1Click(Sender);
    n := 0; m := 0;
   for j:=1 to perulangan do
     begin
        n := n+Objek[j].x;
        m := m+Objek[j].y;
     end;
     n := n div perulangan;
     m := m div perulangan;
   for i:=1 to 5 do
   begin
      Objek[i].x:=Objek[i].x-n;
     Objek[i].y:=Objek[i].y-m;
     Sdt:=15*pi/180;
     TempObjek[i].x:=round(Objek[i].x*cos(Sdt)-Objek[i].y*sin(Sdt));
     TempObjek[i].y:=round(Objek[i].x*sin(Sdt)+Objek[i].y*cos(Sdt));
     Objek[i]:=TempObjek[i];
     Objek[i].x:=Objek[i].x+n;
     Objek[i].y:=Objek[i].y+m
   end;
   FormShow(Sender);
end;

procedure TForm1.RkiriClick(Sender: TObject);
  var
    TempObjek: array[1..6] of elemen;
    n,m : integer;
    Sdt: real;
  begin
    Button1Click(Sender);
    n := 0; m := 0;
   for j:=1 to perulangan do
     begin
        n := n+Objek[j].x;
        m := m+Objek[j].y;
     end;
     n := n div perulangan;
     m := m div perulangan;
   for i:=1 to 5 do
   begin
      Objek[i].x:=Objek[i].x-n;
     Objek[i].y:=Objek[i].y-m;
     Sdt:=-15*pi/180;
     TempObjek[i].x:=round(Objek[i].x*cos(Sdt)-Objek[i].y*sin(Sdt));
     TempObjek[i].y:=round(Objek[i].x*sin(Sdt)+Objek[i].y*cos(Sdt));
     Objek[i]:=TempObjek[i];
     Objek[i].x:=Objek[i].x+n;
     Objek[i].y:=Objek[i].y+m
   end;
   FormShow(Sender);

end;

Procedure TForm1.BoundaryFill(x,y,fill,boundary:Integer);
var
  current:Integer;
begin
  current:=Image1.Canvas.Pixels[x,y];
  if(current<>boundary) and (current<>fill) then
  begin
    Image1.Canvas.Pixels[x,y]:=fill;
    Image1.Refresh;
    BoundaryFill(x+1,y,fill,boundary);
    BoundaryFill(x-1, y,fill,boundary);
    BoundaryFill(x,y+1,fill,boundary);
    BoundaryFill(x,y-1,fill,boundary);
  end;
end;

Procedure TForm1.FloodFill(x,y,fill,boundary:Integer);
begin
  if(Image1.Canvas.Pixels[x,y]=boundary)then
  begin
    Image1.Canvas.Pixels[x,y]:=fill;
    Image1.Refresh;
    BoundaryFill(x+1,y,fill,boundary);
    BoundaryFill(x-1, y,fill,boundary);
    BoundaryFill(x,y+1,fill,boundary);
    BoundaryFill(x,y-1,fill,boundary);
  end;
end;

procedure TForm1.FormShow(sender:TObject);
begin
  if (perulangan <> 2) then
   begin
     Image1.Canvas.MoveTo(Objek[perulangan].x,Objek[perulangan].y);
     for i:=1 to perulangan do
     begin
       Image1.Canvas.LineTo(Objek[i].x,Objek[i].y);
     end;
   end;
  if (perulangan = 2) then
   begin
      Image1.Canvas.Ellipse(Objek[1].x,Objek[1].y,Objek[2].x,Objek[2].y);
   end;
end;

end.

